import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import { validate } from 'class-validator'

import User from '../../models/user'

export default {
  async index(request: Request, response: Response) {
    const institutionRepository = getRepository(User)

    const users = await institutionRepository.find()

    return response.status(200).json(users)
  },
  async show(request: Request, response: Response) {
    const { id } = request.params

    const repository = getRepository(User)
    const users = await repository.findOneOrFail(id, { relations: ['votes'] })

    return response.status(200).json(users)
  },
  async create(request: Request, response: Response) {
    const { userName, email, password } = request.body

    const data = { userName, email, password, role: 'ADMIN' }

    const UserRepository = getRepository(User)

    const user = UserRepository.create(data)

    const errors = await validate(user)

    if (errors.length > 0) throw errors[0]

    const userCreate = await UserRepository.save(user)

    return response.status(201).json({ userCreate })
  },
  async store(request: Request, response: Response) {
    const { userName, email, password } = request.body

    const data = { userName, email, password }

    const UserRepository = getRepository(User)

    const user = UserRepository.create(data)

    const errors = await validate(user)

    if (errors.length > 0) throw errors[0]

    const userCrate = await UserRepository.save(user)

    return response.status(201).json(userCrate)
  },
  async update(request: Request, response: Response) {
    const { userId } = response.locals.jwtPayload

    const repository = getRepository(User)

    if (userId !== request.params.id) {
      return response.status(403).send()
    }

    await repository.update({ id: request.params.id }, request.body)

    return response.status(204).send()
  }
}
