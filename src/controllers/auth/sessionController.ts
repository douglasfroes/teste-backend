import { Request, Response } from 'express'
import * as jwt from 'jsonwebtoken'
import { getRepository } from 'typeorm'

import config from '../../config/auth'
import User from '../../models/user'

export default {
  async store(request: Request, response: Response) {
    const { email, password } = request.body

    if (!(email && password)) {
      response.status(400).send()
    }

    const userRepository = getRepository(User)

    let user: User
    let passwordCompare: boolean

    try {
      user = await userRepository.findOneOrFail({
        select: ['userName', 'password', 'id', 'role'],
        where: { email }
      })
      passwordCompare = await user.comparePassword(password)

      if (!passwordCompare) {
        response.status(404).send()
      }

      const token = jwt.sign(
        { userId: user.id, userName: user.userName },
        config.secret as string,
        { expiresIn: '368d' }
      )

      response.status(200).json({
        token,
        user: {
          userId: user.id,
          userName: user.userName,
          role: user.role
        }
      })
    } catch (error) {
      response.status(404).send()
    }
  }
}
