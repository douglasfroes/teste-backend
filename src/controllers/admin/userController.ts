import { Request, Response } from 'express'
import { getRepository } from 'typeorm'

import User from '../../models/user'

export default {
  async delete(request: Request, response: Response) {
    const repository = getRepository(User)

    const user = await repository.findOneOrFail(
      request.params.id
    )

    user.disabled = true

    await repository.save(user)

    return response.status(204).send()
  }
}
