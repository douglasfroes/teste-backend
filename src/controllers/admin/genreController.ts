import { Request, Response } from 'express'
import { getRepository } from 'typeorm'

import Genre from '../../models/genre'

export default {
  async create(request: Request, response: Response) {
    const { title } = request.body

    const data = { title }

    const repository = getRepository(Genre)

    const genre = repository.create(data)

    const genreCreate = await repository.save(genre)

    return response.status(201).json(genreCreate)
  },
  async update(request: Request, response: Response) {
    const repository = getRepository(Genre)

    await repository.update(
      { id: request.params.id },
      request.body
    )

    return response.status(204).send()
  }
}
