import { Request, Response } from 'express'
import { getRepository } from 'typeorm'

import Movie from '../../models/movie'

export default {
  async create(request: Request, response: Response) {
    const { title, url, about, director, actors, genre } = request.body

    const data = { title, url, about, director, actors, genre }

    const Repository = getRepository(Movie)

    const movie = Repository.create(data)

    const movieCrate = await Repository.save(movie)

    return response.status(201).json(movieCrate)
  },
  async update(request: Request, response: Response) {
    const repository = getRepository(Movie)

    await repository.update({ id: request.params.id }, request.body)

    return response.status(204).send()
  },
  async delete(request: Request, response: Response) {
    const repository = getRepository(Movie)

    const movie = await repository.findOneOrFail(
      request.params.id
    )

    movie.disabled = true

    await repository.save(movie)

    return response.status(204).send()
  }
}
