import { Request, Response } from 'express'
import { getRepository, ILike } from 'typeorm'

import Movie from '../../models/movie'

export default {
  async index(request: Request, response: Response) {
    const { title, director, genre, actors }:any = request.query
    let movies:Movie[]

    const repository = getRepository(Movie)

    if (title) {
      movies = await repository.find({ title: ILike(`%${title}%`) })
    } else if (director) {
      movies = await repository.find({ director: ILike(`%${director}%`) })
    } else if (actors) {
      movies = await repository.find({ actors: ILike(`%${actors}%`) })
    } else if (genre) {
      movies = await repository
        .createQueryBuilder('movie')
        .leftJoinAndSelect('movie.genre', 'genre')
        .where('genre.title like :title', {
          title: `%${genre}%`
        })
        .getMany()
    } else {
      movies = await repository.find()
    }

    return response.status(200).json(movies)
  },
  async show(request: Request, response: Response) {
    const repository = getRepository(Movie)

    const movie = await repository.findOneOrFail(request.params.id,
      { relations: ['votes', 'genre'] })

    let vote = 0

    movie.votes.forEach(item => { vote += item.vote })

    return response.status(200).json(
      {
        id: movie.id,
        title: movie.title,
        url: movie.url,
        about: movie.about,
        director: movie.director,
        actors: movie.actors,
        genre: movie.genre.title,
        vote: vote / movie.votes.length
      })
  }
}
