import { Request, Response } from 'express'
import { getRepository } from 'typeorm'

import Genre from '../../models/genre'

export default {
  async index(request: Request, response: Response) {
    const repository = getRepository(Genre)

    const genres = await repository.find()

    return response.status(200).json(genres)
  },
  async show(request: Request, response: Response) {
    const repository = getRepository(Genre)

    const genre = await repository.findOneOrFail(request.params.id, {
      relations: ['movies']
    })

    return response.status(200).json(genre)
  }
}
