import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import { validate } from 'class-validator'

import Vote from '../../models/vote'

export default {
  async index(request: Request, response: Response) {
    const repository = getRepository(Vote)

    const votes = await repository.find()

    return response.json(votes)
  },

  async show(request: Request, response: Response) {
    const repository = getRepository(Vote)

    const vote = await repository.findOneOrFail(request.params.id,
      { relations: ['movie', 'user'] }
    )

    return response.json(vote)
  },

  async create(request: Request, response: Response) {
    const { vote, movie } = request.body
    const { userId } = response.locals.jwtPayload

    const data = { vote, movie, user: userId }

    const repository = getRepository(Vote)

    const vote2 = repository.create(data)

    const errors = await validate(vote2)

    if (errors.length > 0) throw errors[0]

    const voteCreate = await repository.save(vote2)

    return response.status(201).json(voteCreate)
  },
  async update(request: Request, response: Response) {
    const repository = getRepository(Vote)

    await repository.update(
      { id: request.params.id },
      request.body
    )

    return response.status(204).send()
  }
}
