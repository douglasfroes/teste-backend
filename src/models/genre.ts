import {
  Entity,
  Column,
  JoinColumn,
  ObjectID,
  PrimaryGeneratedColumn,
  OneToMany
} from 'typeorm'

import Movie from './movie'

@Entity('genre')
export default class Assignment {
  @PrimaryGeneratedColumn()
  id: ObjectID

  @Column('varchar')
  title: string

  @OneToMany(() => Movie, movie => movie.genre)
  @JoinColumn()
  movies: Movie[]
}
