import {
  Entity,
  Column,
  ManyToOne,
  JoinColumn,
  ObjectID,
  PrimaryGeneratedColumn
} from 'typeorm'

import { IsInt, Max, Min } from 'class-validator'
import Movie from './movie'
import User from './user'

@Entity('vote')
export default class Assignment {
  @PrimaryGeneratedColumn()
  id: ObjectID

  @IsInt()
  @Min(0)
  @Max(4)
  @Column('int')
  vote: number

  @ManyToOne(() => Movie, movie => movie.votes, { nullable: false })
  @JoinColumn()
  movie: Movie

  @ManyToOne(() => User, user => user.votes, { nullable: false })
  @JoinColumn()
  user: User
}
