import {
  Entity,
  Column,
  BeforeInsert,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  ObjectID,
  OneToMany
} from 'typeorm'
import { Length, IsEmail } from 'class-validator'

import bcrypt from 'bcrypt'
import Vote from './vote'

@Entity('user')
export default class User {
  @PrimaryGeneratedColumn()
  id: ObjectID

  @Column('varchar', {
    unique: true
  })
  userName: string

  @IsEmail()
  @Column('varchar', { unique: true })
  email: string

  @Length(8, 20)
  @Column('varchar', { select: false })
  password: string

  @Column('boolean')
  disabled: boolean=false

  @Column('varchar', { nullable: true })
  role: string

  @Column()
  @CreateDateColumn()
  createdAt: Date

  @Column()
  @UpdateDateColumn()
  updatedAt: Date

  @OneToMany(type => Vote, vote => vote.movie, {
    cascade: true,
    onUpdate: 'CASCADE'
  })
  votes: Vote[]

  @BeforeInsert()
  async hasPassword() {
    this.password = await bcrypt.hash(this.password, 10)
  }

  async comparePassword(text: string): Promise<boolean> {
    return await bcrypt.compare(text, this.password)
  }
}
