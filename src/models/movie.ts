import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
  ObjectID,
  ManyToOne
} from 'typeorm'

import Vote from './vote'
import Genre from './genre'

@Entity('movie')
export default class User {
  @PrimaryGeneratedColumn()
  id: ObjectID

  @Column('varchar')
  title: string

  @Column('varchar')
  url: string

  @Column('varchar')
  about: string

  @Column('varchar')
  director: string

  @Column('varchar')
  actors: string

  @Column('boolean')
  disabled: boolean=false

  @Column()
  @CreateDateColumn()
  createdAt: Date

  @Column()
  @UpdateDateColumn()
  updatedAt: Date

  @ManyToOne(() => Genre, genre => genre.movies)
  @JoinColumn()
  genre: Genre

  @OneToMany(type => Vote, vote => vote.movie, {
    cascade: true,
    onUpdate: 'CASCADE'
  })
  votes: Vote[]
}
