import { Router } from 'express'

import voteController from '../controllers/app/vote'
import movieController from '../controllers/app/movieController'
import genreController from '../controllers/app/genreController'
import userController from '../controllers/app/userController'

import jwt from '../middlewares/jwt'

const routes = Router()

routes.use(jwt)

routes.get('/vote', voteController.index)
routes.put('/vote/:id', voteController.update)
routes.post('/vote', voteController.create)
routes.put('/vote/:id', voteController.update)

routes.get('/movie', movieController.index)
routes.get('/movie/:id', movieController.show)

routes.get('/genre', genreController.index)
routes.get('/genre/:id', genreController.show)

routes.put('/user/:id', userController.update)
routes.delete('/user/:id', userController.delete)

export default routes
