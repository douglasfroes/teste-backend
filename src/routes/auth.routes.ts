import { Router } from 'express'
import userController from '../controllers/auth/userController'
import sessionController from '../controllers/auth/sessionController'

const routes = Router()

routes.post('/admin/user', userController.create)
routes.post('/user', userController.store)

routes.get('/users', userController.index)
routes.get('/user/:id', userController.show)

routes.post('/session', sessionController.store)

export default routes
