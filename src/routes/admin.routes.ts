import { Router } from 'express'

import movieController from '../controllers/admin/movieController'
import genreController from '../controllers/admin/genreController'
import userController from '../controllers/admin/userController'

import role from '../middlewares/role'
import jwt from '../middlewares/jwt'

const routes = Router()

routes.use(jwt)
routes.use(role(['ADMIN']))

routes.post('/movie', movieController.create)
routes.put('/movie/:id', movieController.update)
routes.put('/movie/:id', movieController.delete)

routes.post('/genre', genreController.create)
routes.put('/genre/:id', genreController.update)

routes.delete('/user/:id', userController.delete)

export default routes
