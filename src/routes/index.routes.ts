import { Router } from 'express'
import auth from './auth.routes'
import app from './app.routes'
import admin from './admin.routes'

const routes = Router()

routes.get('/', (req, res) => res.status(200).json({ api: 'on' }))

routes.use('/auth', auth)
routes.use('/app', app)
routes.use('/admin', admin)

export default routes
