import { ErrorRequestHandler } from 'express'
import { EntityNotFoundError, QueryFailedError } from 'typeorm'
import { ValidationError } from 'class-validator'

const errorHandler: ErrorRequestHandler = (error, request, response, next) => {
  if (error instanceof EntityNotFoundError) {
    return response.status(400).json(error)
  }

  if (error instanceof QueryFailedError) {
    return response.status(400).json(error)
  }

  if (error instanceof ValidationError) {
    return response.status(400).json(error)
  }

  console.log(error)

  return response.status(500).json({ message: 'Internal server error' })
}

export default errorHandler
