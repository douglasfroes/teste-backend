import { MigrationInterface, QueryRunner } from 'typeorm'

export class db1614885818182 implements MigrationInterface {
    name = 'db1614885818182'

    public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query('CREATE TABLE "user" ("id" SERIAL NOT NULL, "userName" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "disabled" boolean NOT NULL, "role" character varying, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_da5934070b5f2726ebfd3122c80" UNIQUE ("userName"), CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))')
      await queryRunner.query('CREATE TABLE "vote" ("id" SERIAL NOT NULL, "vote" integer NOT NULL, "movieId" integer NOT NULL, "userId" integer NOT NULL, CONSTRAINT "PK_2d5932d46afe39c8176f9d4be72" PRIMARY KEY ("id"))')
      await queryRunner.query('CREATE TABLE "movie" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "url" character varying NOT NULL, "about" character varying NOT NULL, "director" character varying NOT NULL, "actors" character varying NOT NULL, "disabled" boolean NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "genreId" integer, CONSTRAINT "PK_cb3bb4d61cf764dc035cbedd422" PRIMARY KEY ("id"))')
      await queryRunner.query('CREATE TABLE "genre" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, CONSTRAINT "PK_0285d4f1655d080cfcf7d1ab141" PRIMARY KEY ("id"))')
      await queryRunner.query('ALTER TABLE "vote" ADD CONSTRAINT "FK_499e10a58992a610cfbaf8f18ed" FOREIGN KEY ("movieId") REFERENCES "movie"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
      await queryRunner.query('ALTER TABLE "vote" ADD CONSTRAINT "FK_f5de237a438d298031d11a57c3b" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
      await queryRunner.query('ALTER TABLE "movie" ADD CONSTRAINT "FK_3aaeb14b8d10d027190f3b159e5" FOREIGN KEY ("genreId") REFERENCES "genre"("id") ON DELETE NO ACTION ON UPDATE NO ACTION')
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query('ALTER TABLE "movie" DROP CONSTRAINT "FK_3aaeb14b8d10d027190f3b159e5"')
      await queryRunner.query('ALTER TABLE "vote" DROP CONSTRAINT "FK_f5de237a438d298031d11a57c3b"')
      await queryRunner.query('ALTER TABLE "vote" DROP CONSTRAINT "FK_499e10a58992a610cfbaf8f18ed"')
      await queryRunner.query('DROP TABLE "genre"')
      await queryRunner.query('DROP TABLE "movie"')
      await queryRunner.query('DROP TABLE "vote"')
      await queryRunner.query('DROP TABLE "user"')
    }
}
