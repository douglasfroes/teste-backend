import { Request, Response, NextFunction } from 'express'
import * as jwt from 'jsonwebtoken'
import config from '../config/auth'

const checkJwt = (req: Request, res: Response, next: NextFunction) => {
  // Get the jwt token from the head
  const token = <string>req.headers.authorization?.slice(7)
  let jwtPayload

  // Try to validate the token and get data
  try {
    jwtPayload = <any>jwt.verify(token, config.secret as string)
    res.locals.jwtPayload = jwtPayload
    // console.log(jwtPayload);
  } catch (error) {
    // If token is not valid, respond with 401 (unauthorized)
    res.status(401).send()
    return
  }

  res.setHeader('token', token)

  next()
}

export default checkJwt
