import { Request, Response, NextFunction } from 'express'
import { getRepository } from 'typeorm'

import User from '../models/user'

const role = (roles: Array<string>) => {
  return async(req: Request, res: Response, next: NextFunction) => {
    // Get the user ID from previous midleware
    const id = res.locals.jwtPayload.userId

    let user: User

    try {
      // Get user role from the database
      const userRepository = getRepository(User)

      user = await userRepository.findOneOrFail(id)

      // Check if array of authorized roles includes the user's role
      if (roles.indexOf(user.role) > -1) next()

      else res.status(401).send()
    } catch (id) {
      res.status(401).send()
    }
  }
}

export default role
