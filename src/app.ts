import express from 'express'
import cors from 'cors'
import 'express-async-errors'
import dotenv from 'dotenv'
import 'reflect-metadata'
import 'module-alias/register'

import './database/connection'
import routes from './routes/index.routes'
import errorHandler from './errors/handler'

dotenv.config()
const app = express()

app.use(cors())
app.use(express.json())
app.use(routes)
app.use(errorHandler)

export { app }
