module.exports = {
  type: 'postgres',
  url: process.env.DATABASE_URL,
  synchronize: true,
  logging: false,
  migrations: process.env.NODE_ENV === 'production' ? ['./dist/database/migrations/*.js'] : ['./src/database/migrations/*.ts'],
  entities: process.env.NODE_ENV === 'production' ? ['./dist/models/*.js'] : ['./src/models/*.ts'],
  cli: {
    migrationsDir: './src/database/migrations'
  }
}
